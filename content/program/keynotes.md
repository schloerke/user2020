+++
title = "Keynote Speakers"
date = "2020-01-25T00:00:00"
tags = ["Keynotes", "Speakers"]
categories = ["Keynotes"]
+++

## Erin LeDell, Ph.D.

Erin LeDell is the Chief Machine Learning Scientist at H2O.ai, the company that produces the open source, distributed machine learning platform, H2O. She received her Ph.D. from UC Berkeley where her research focused on machine learning and computational statistics. Erin is the founder of Women in Machine Learning & Data Science (WiMLDS), and one of the co-founders of R-Ladies Global. Erin is also an active developer of a number of R packages, including `h2o` , `cvAUC`, and `rsparkling`.

{{< figure src="/news/erin.png" caption="Erin LeDell" >}}

## Martin Mächler, Ph.D.

Martin Mächler is the Secretary General of the R Foundation and a member of the R Core Development team. He is a Senior Scientist and Lecturer at ETH Zurich's Seminar for Statistics. Martin is also an active developer of over twenty R packages, including `Matrix` and `cluster`.

{{< figure src="/news/martin.png" caption="Martin Mächler" >}}

## Noam Ross, Ph.D.

Noam Ross is a Senior Research Scientist and computational disease ecologist at EcoHealth Alliance. He received his Ph.D. from UC Davis, is a founding editor for software review at ROpenSci, and is an instructor for the Software Carpentry and Data Carpentry foundations. Noam is also an active developer of a number of R packages, including `fasterize` and `redoc`.

{{< figure src="/news/noam.png" caption="Noam Ross" >}}

## Gabriela de Queiroz, Ph.D.

Gabriela de Queiroz is a Sr. Engineering & Data Science Manager/Sr. Developer Advocate at IBM where she leads and manages a team of data scientists and software engineers to contribute to open source and artificial intelligence projects. She works in different open source projects and is actively involved with several organizations to foster an inclusive community. She is the founder of R-Ladies, a worldwide organization for promoting diversity in the R community with more than 165 chapters in 45+ countries. She is now working to make AI more diverse and inclusive in her new organization, AI Inclusive. She has worked in several startups where she built teams, developed statistical and machine learning models and employed a variety of techniques to derive insights and drive data-centric decisions. 

{{< figure src="/news/gabriela.png" caption="Gabriela de Queiroz" >}}

## Amelia McNamara, Ph.D.

Amelia McNamara is an assistant professor in the Department of Computer & Information Sciences at the University of St Thomas, in Minnesota.  She received her PhD in statistics from UCLA. Her research lies at the intersection of statistics education and statistical computing, with the goal of making it easier for everyone to learn and do statistics. She has been involved in the development of R packages such as mobilizr (for high school students learning data science) and skimr (frictionless summary statistics). 

{{< figure src="/news/amelia.png" caption="Amelia McNamara" >}}

## Latanya Sweeney, Ph.D.

Professor of government and technology in residence at Harvard University, Editor-in-Chief of [Technology Science](https://techscience.org), director and founder of the [Data Privacy Lab](https://dataprivacylab.org), former Chief Technology Officer at the U.S. Federal Trade Commission and Distinguished Career Professor of Computer Science, Technology and Policy at Carnegie Mellon University, Latanya Sweeney has 3 patents, more than [100 academic publications](http://latanyasweeney.org/publications.html), and her work is explicitly cited in two U.S. regulations, including the U.S. federal medical privacy regulation (known as HIPAA). She is a recipient of the prestigious Louis D. Brandeis Privacy Award, the American Psychiatric Association's Privacy Advocacy Award, an elected fellow of the American College of Medical Informatics, and has testified before government bodies worldwide. She earned her PhD in computer science from MIT in 2001, being the first black woman to do so, and her undergraduate degree in computer science from Harvard University. Dr. Sweeney creates and uses technology to assess and solve societal, political and governance problems, and teaches others how to do the same, often using R. More information is available at [her website](latanyasweeney.org).
 
{{< figure src="/news/latanya.png" caption="Latanya Sweeney" >}}