+++
title = "Venue"
description = "Details about location and setting"
keywords = ["location","travel","country","accommodation"]
+++

### <i class="fas fa-bed"></i> Stay at the Venue
The conference venue, the [Marriott St. Louis Grand Hotel](https://www.marriott.com/hotels/travel/stlmg-marriott-st-louis-grand/) includes a free-standing conference center as well as a historic hotel next door. useR! 2020 attendees will receive discounted room rates from July 6th through July 10th. The rate for attendees will be $159 per night, plus applicable state and local taxes. Access to a priority booking site for hotel reservations will be available when [registration opens](/registration/).

<br>
### <i class="fas fa-map-marker-alt"></i> Location
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3116.7976897612784!2d-90.19444628466478!3d38.63053497834059!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87d8b318c99be2d9%3A0xaa40e422becdbdae!2sMarriott%20St.%20Louis%20Grand!5e0!3m2!1sen!2sus!4v1568062604639!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

<!-- go to google maps, select your location, and copy and paste the 'embed' frame into the file where you want it to appear -->
