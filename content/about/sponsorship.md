+++
title = "Sponsorship"
description = "Sponsorship Information"
keywords = ["about","organisers","staff","R consortium", "sponsorship"]
+++
## <i class="fas fa-eye"></i> Sponsorship at a Glance

1. Full conference sponsorship opportunities are available at the following levels:

    a. Diamond - $25,000

    b. Platinum - $15,000

    c. Gold - $10,000

    d. Silver - $5,000

    e. Bronze - $2,500

    f. Violet (businesses with fewer than 30 employees) - $1,500

2. Diversity Scholarship - available in increments of $1,000 - this will underwrite a grant
fund to support attendance by members of underrepresented groups. Sponsors will
be highlighted at a dedicated networking event.
3. Event specific opportunities, such as sponsoring a meal or social event, are also
available. Specific levels and benefits can be discussed with the sponsorship team.
4. The conference’s Geospatial track has specific sponsorship opportunities. Please
contact the sponsorship team for details.